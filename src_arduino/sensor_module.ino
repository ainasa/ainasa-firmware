//Proyecto AiNasa hackaton San Jurjo  Badia
//27 y 28 de febrero 2016
//Equipo:
// David Constela 
// Adan Doan Kim
// Jose Manuel Cigues
// Jorge Martinez
//
// Led Rojo: Nasa fuera del agua
// Led Verde: Capturo algo
// Led Azul: Sistema activado

//ai'nasa
//Copyright 2016 by Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames.
//All rights reserved.

//This project remains the property of Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames. Dissemination, reproduction or transmission of any part
//of this project in any form or by any means, including copying, by electronic or mechanical methods is strictly
//forbidden unless prior written permission is obtained from ai'nasa team.


//Contantes
const int buttonPin = 2;     // Numero de Pin para definir el boton de salida de agua
const int ledPin =  12;      // Numero de Pin para definir el Led de color Rojo

//Variable
int buttonState = 0;         // Estado del Sensor de nasa fuera del agua 
int Acu_Eco = 1;                // valor del sonar
int Acu_agua = 1;                // valor sensor de dentro del agua
// Libreria del Sonar
 
#include <Ultrasonic.h>

//Definicion de 
Ultrasonic ultrasonic(9,8); // (Trig PIN,Echo PIN)
Ultrasonic ultrasonic2(6,7); // (Trig PIN,Echo PIN)
Ultrasonic ultrasonic3(11,12); // (Trig PIN,Echo PIN)
Ultrasonic ultrasonic4(3,2); // (Trig PIN,Echo PIN)


void setup() {
  Serial.begin(9600); 
    pinMode(2, INPUT);
  pinMode(4, OUTPUT);//azul
  pinMode(7, OUTPUT);//rojo
   pinMode(12, OUTPUT);//verde

     // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  
}

void loop()
{
  digitalWrite(12, LOW);
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
    Acu_agua=1;
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
    Acu_agua=0;
  }
  
  digitalWrite(7, HIGH); 

Acu_Eco=ultrasonic.Ranging(CM);

  if ( Acu_Eco>=12) {
    // turn LED on:
 digitalWrite(4, LOW);
  
  } else {
    // turn LED off:
    digitalWrite(4, HIGH);
    
   
  }

   
  
 Serial.println("S"); // 
  Serial.println(Acu_Eco); // 
  Serial.println(Acu_agua); // 
  delay(1000);

  
}
