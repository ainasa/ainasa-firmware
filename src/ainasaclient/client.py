"""
ai'nasa
Copyright 2015 by Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames.
All rights reserved.

This project remains the property of Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames. Dissemination, reproduction or transmission of any part
of this project in any form or by any means, including copying, by electronic or mechanical methods is strictly
forbidden unless prior written permission is obtained from ai'nasa team.
"""
import os
import json
import requests
import datetime

try:
    nasaID = os.environ['NASAID']
    if nasaID == None or len(nasaID) == 0:
        raise Exception('Missing nasa id environ var')
except:
    print "Missing nasa id environ var: NASAID"
    exit()

def sendDetection(detect, sumerged):
    info = {'detection': {'height': detect, 'width': detect}}
    mDate = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    info['last_log'] = mDate
    
    if not sumerged:
        info['last_out'] = mDate 

    response = requests.put('http://172.16.3.125:5000/nasa/' + nasaID + '/', data=json.dumps(info))
    if (response.status_code != 200):
        print "ERROR: Error al enviar los datos al servidor (status code %s)!\n" % response.status_code
    else:
        print "Envío al servidor OK\n"