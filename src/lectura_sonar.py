"""
ai'nasa
Copyright 2015 by Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames.
All rights reserved.

This project remains the property of Jorge Martinez Santiago, Jose Manuel Ciges Regueiro, David Constenla Rodriguez and Adam Anh Doan Kim Carames. Dissemination, reproduction or transmission of any part
of this project in any form or by any means, including copying, by electronic or mechanical methods is strictly
forbidden unless prior written permission is obtained from ai'nasa team.
"""
import serial
import time
from ainasaclient import client

ser = serial.Serial('/dev/ttyMCC',9600,timeout=1)
ser.flushOutput()

print 'Serial connected'

# Reducimos el ritmo de envío, enviamos cada 4 lecturas
ritmo_envio = 4
# Enviador al servidor o no
send_server = False;

i = 0
while True:
	distancia = None;
	ocupada = None;
	cadena = ser.readline().rstrip();
	#print "Cadena: %s" % cadena;
	if cadena:
		if cadena == "S":
			distancia = ser.readline().rstrip();
			ocupada = ser.readline().rstrip();

			i = i + 1
			if i % ritmo_envio == 0:
				distancia = int(distancia);
				ocupada = True if (ocupada == "1") else False;
				print "Señal de sonar: distancia %d - ocupada %s" % (distancia, "si" if ocupada else "no")
				if send_server:
					client.sendDetection(distancia, ocupada)
				i = 0
